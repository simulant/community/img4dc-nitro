#ifndef __UTILS_HPP__
#define __UTILS_HPP__

#include <iostream>
#include <cstring>
#include <algorithm>
#include <memory>

#define RETURN_STR_UNKNOWN "<UNKNOWN>";
#define RETURN_STR(val, e) {if (val == e) {return #e;}}

std::string change_file_extension( std::string const & filename, std::string const& extension );
bool starts_with( std::string const & value, std::string const & prefix );
bool ends_with( std::string const & value, std::string const & suffix );
void to_lower( std::string & str );
void to_upper( std::string & str );
std::streamsize get_file_size( std::istream & infile );
void write_null_block( std::ostream & outfile, unsigned long block_size );

#endif // __UTILS_HPP__
