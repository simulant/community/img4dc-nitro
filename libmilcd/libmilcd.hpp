#ifndef __LIBMILCD_HPP__
#define __LIBMILCD_HPP__

#include <string>
#include <list>

#include <fstream>
#include <iostream>
#include <memory>
#include <ios>
#include <limits>

#include <kazsignal/kazsignal.h>

#include "libedc.h"
#include "utils.hpp"

#define FILE_EXTENSION_AUDIO "RAW"
#define FILE_EXTENSION_DATA "ISO"
#define FILE_EXTENSION_DISCJUGGLER "CDI"
#define FILE_EXTENSION_ALCOHOL "MDF"
#define FILE_EXTENSION_ALCOHOL_HEADER "MDS"

#define MINIMAL_TRACK_SIZE_BLOCKS 300

#define AUDIO_SECTOR_SIZE 2352
#define AUDIO_SECTOR_START_SIZE 0x18

enum MilCDImageMode {
    MIL_CD_BUILDER_MODE_INVALID,
    MIL_CD_BUILDER_MODE_DISCJUGGLER,
    MIL_CD_BUILDER_MODE_ALCOHOL
};

enum MilCDImageFormat {
    MIL_CD_IMAGE_FORMAT_INVALID,
    MIL_CD_IMAGE_FORMAT_AUDIO_DATA,
    MIL_CD_IMAGE_FORMAT_DATA_DATA
};

enum MilCDTrackType {
    MIL_CD_TRACK_TYPE_INVALID,
    MIL_CD_TRACK_TYPE_AUDIO,
    MIL_CD_TRACK_TYPE_DATA
};

class MilCDTrack {
    public:
        MilCDTrack() {}
        MilCDTrack(std::string const & file_name) { this->set_file_name(file_name); }
        virtual ~MilCDTrack() { if(this->file_) this->file_->close(); };

        std::string get_file_name() { return this->file_name_; };
        void set_file_name(std::string const & file_name);

        bool is_valid_track() { return this->file_name_.length() && this->file_size_; };

        std::shared_ptr<std::ifstream> get_file_stream() { return this->file_; };
        std::streamsize get_track_size() { return this->file_size_; };
        MilCDTrackType get_track_type() { return this->track_type_; };

    protected:
        void initialize();
        void detect_track_type_from_extension();

    private:
        std::shared_ptr<std::ifstream> file_;
        std::streamsize file_size_;
        std::string file_name_;
        MilCDTrackType track_type_;
};

class MilCDSession {
    public:
        MilCDSession() {};
        virtual ~MilCDSession() {};

        bool is_valid_audio_tracks() { return this->audio_tracks_.size(); };
        void add_audio_track(std::string const & audio_file);
        std::shared_ptr<MilCDTrack> get_audio_track(int track_index) { return this->audio_tracks_[track_index]; };
        int get_audio_track_count() { return this->audio_tracks_.size(); };

        bool is_valid_data_track() { return this->data_track_.is_valid_track(); };
        void set_data_track(std::string const & input_file) { this->data_track_.set_file_name(input_file); };
        MilCDTrack & get_data_track() { return this->data_track_; };

    private:
        std::vector<std::shared_ptr<MilCDTrack>> audio_tracks_;
        MilCDTrack data_track_;
};

class MilCDImageGenerator {
    DEFINE_SIGNAL(sig::signal<void (std::streamsize, std::streamsize, unsigned int)>, progress_signal);

    public:
        MilCDImageGenerator();
        virtual ~MilCDImageGenerator();

        void build();

        void set_output_file(std::string const & output_file) { this->output_image_file_ = output_file; this->detect_image_mode_from_output_extension(); };
        std::string get_output_file() { return this->output_image_file_; };

        MilCDImageMode get_output_image_mode() { return this->mode_; }
        MilCDImageFormat get_output_image_format() { return this->format_; }

        MilCDSession& get_first_session() { return this->first_session_; }
        MilCDSession& get_second_session() { return this->second_session_; }

    protected:
        unsigned int process_audio_tracks(MilCDSession & session);
        unsigned int process_data_track(MilCDSession & session);
        unsigned int input_to_output(MilCDTrack & track);

    private:
        std::string output_image_file_;

        MilCDImageMode mode_;
        MilCDImageFormat format_;

        std::ofstream outfile_{nullptr};
        unsigned long current_lba_{0};

        MilCDSession first_session_;
        MilCDSession second_session_;

        void detect_image_mode_from_output_extension();
};

std::string conv_milcd_image_mode_to_string(int value);

#endif // __LIBMILCD_HPP__
