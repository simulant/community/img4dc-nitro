#include "libmilcd.hpp"

MilCDImageGenerator::MilCDImageGenerator() {

}

MilCDImageGenerator::~MilCDImageGenerator() {

}

void
MilCDTrack::set_file_name(std::string const & file_name) {
    this->file_name_ = file_name;
    this->initialize();
}

void
MilCDTrack::detect_track_type_from_extension() {
    this->track_type_ = MIL_CD_TRACK_TYPE_INVALID;

    std::string track_file = this->get_file_name();
    to_upper(track_file);

    if (ends_with(track_file, FILE_EXTENSION_AUDIO)) {
        this->track_type_ = MIL_CD_TRACK_TYPE_AUDIO;
    } else if (ends_with(track_file, FILE_EXTENSION_DATA)) {
        this->track_type_ = MIL_CD_TRACK_TYPE_DATA;
    }
}

void
MilCDTrack::initialize() {
#ifdef DEBUG
    std::cout << "MilCDTrack initialize: " << this->file_name_ << std::endl;
#endif // DEBUG

    // Detect the extension
    this->detect_track_type_from_extension();

    // Opening the file stream
    this->file_ = std::make_shared<std::ifstream>( this->get_file_name(), std::ifstream::binary );

    // Checking if the source ISO file exists
    if (!this->file_->is_open()) {
        throw std::runtime_error("input file not found");
    }

    // Get input file size
    this->file_size_ = get_file_size( *this->file_ );
}

void
MilCDSession::add_audio_track(std::string const & audio_file) {
    std::shared_ptr<MilCDTrack> audio_track = std::make_shared<MilCDTrack>(audio_file);
//    MilCDTrack audio_track(audio_file);
    this->audio_tracks_.push_back(audio_track); /* HERE */
#ifdef DEBUG
    std::cout << "audio_file: " << audio_track->get_file_name() << std::endl;
    std::cout << "  " << audio_track->get_file_stream()->good() << std::endl;
    std::cout << "  " << &audio_track << std::endl;
#endif // DEBUG
}

unsigned int
MilCDImageGenerator::input_to_output(MilCDTrack & track) {
    std::streamsize input_total = track.get_track_size();
    std::shared_ptr<std::ifstream> infile = track.get_file_stream();
    MilCDTrackType track_type = track.get_track_type();

#ifdef DEBUG
    std::cout << "Processing track: " << track.get_file_name() << std::endl;
#endif // DEBUG

    std::streamsize buffer_size = 0;
    std::streamsize input_read = 0;

    unsigned int block_count = 0,
        sector_size = 0;

    char *inbuf = nullptr;
    std::streamsize inbuf_max_length = 0;

    char *outbuf = nullptr;
    std::streamsize outbuf_max_length = 0;

    // Buffer for audio tracks
    char audio_buffer[AUDIO_SECTOR_SIZE];

    // Buffer for data tracks
    // This will use the "libedc" from cdrkit
    edc_data data_buffer;

    // Assign input/output buffer
    switch(track_type) {
        case MIL_CD_TRACK_TYPE_AUDIO:
            inbuf = outbuf = audio_buffer;
            sector_size = sizeof(audio_buffer);
            inbuf_max_length = outbuf_max_length = sector_size;
            break;

        case MIL_CD_TRACK_TYPE_DATA:
            inbuf = data_buffer.inbuf;
            outbuf = data_buffer.outbuf;
            inbuf_max_length = sizeof(data_buffer.inbuf);
            sector_size = sizeof(data_buffer.outbuf);
            outbuf_max_length = sector_size;
            break;

        default:
            throw std::invalid_argument("unrecognized MIL-CD track mode");
    }

    unsigned long fail_counter = 0;

    // Process the input file
    do {
        // Compute the read buffer size.
        // We should have a valid ISO file which is composed by n sectors of 2048 bytes.
        // In case of we have some sectors with invalid length (i.e. the ISO isn't a multiple 2048),
        // we'll handle this special case below.
        buffer_size = inbuf_max_length;
        std::streamsize remaining_bytes = (input_total - input_read);
        if (buffer_size > remaining_bytes) {
            buffer_size = remaining_bytes;
        }

        // Read the ISO file
        if (buffer_size && infile->read(inbuf, buffer_size)) {
            // Send the signal (display progress)
            progress_signal_(input_read, input_total, block_count);

            // Compute the blocks count
            block_count++;

            // Handle invalid sectors size
            if (buffer_size != inbuf_max_length) {
                // This is a special case that shouldn't happen!
                // If the sector in the input ISO file is too small, pad it with zeros.
                memset(inbuf + buffer_size, 0, inbuf_max_length - buffer_size);
            }

            // Store how bytes we've read until now
            input_read += infile->gcount();

            // Call the 'libedc' from the cdrkit to compute ECC
            if (track_type == MIL_CD_TRACK_TYPE_DATA) {
                edc_encode_sector(&data_buffer, this->current_lba_);
            }

            // Write the encoded sector with ECC P / ECC Q
            this->outfile_.write(outbuf, outbuf_max_length);
            this->current_lba_++;
        }

        if (infile->fail()) {
            fail_counter++;
        }

    } while (buffer_size && !infile->eof() && (fail_counter < 10));

#ifdef DEBUG
    if (fail_counter > 0) {
        std::cout << "READ FAILED (" << fail_counter << " times)" << std::endl;
    }
#endif // DEBUG

    // Send the last signal
    progress_signal_(input_read, input_total, block_count);

    // Write padding if needed
    while(block_count < MINIMAL_TRACK_SIZE_BLOCKS) {
		write_null_block(this->outfile_, sector_size);
		block_count++;
	}

	// Write post-gap if needed
	if (track_type == MIL_CD_TRACK_TYPE_AUDIO) {
        block_count = block_count + 2;
        write_null_block(this->outfile_, AUDIO_SECTOR_SIZE);
        write_null_block(this->outfile_, AUDIO_SECTOR_SIZE - AUDIO_SECTOR_START_SIZE);
	}

    return block_count;
}

unsigned int
MilCDImageGenerator::process_audio_tracks(MilCDSession & session) {
    if (!session.is_valid_audio_tracks()) {
        return 0;
    }

    unsigned int block_count = 0,
        total_block_count = 0;

    for(int track_index = 0; track_index < session.get_audio_track_count(); track_index++) {

        std::shared_ptr<MilCDTrack> audio_track = session.get_audio_track(track_index);

#ifdef DEBUG
        std::cout << audio_track->get_file_name() << std::endl;
        std::cout << "   " << audio_track->get_file_stream()->good() << std::endl;
        std::cout << "  " << &audio_track << std::endl;
#endif // DEBUG

        // writing the audio section start
        write_null_block(this->outfile_, AUDIO_SECTOR_START_SIZE);

        // writing the audio content
        block_count = this->input_to_output(*audio_track);

        // writing the TAO blocks
        unsigned int tao_padding = block_count + 150;
        while(block_count < tao_padding) {
            write_null_block(this->outfile_, AUDIO_SECTOR_SIZE);
            block_count++;
        }

        // I don't know it it will be interesting for now
        total_block_count += block_count;
    }

    return total_block_count;
}

unsigned int
MilCDImageGenerator::process_data_track(MilCDSession & session) {
    if (!session.is_valid_data_track()) {
        return 0;
    }

    unsigned int block_count = 0;

    MilCDTrack data_track = session.get_data_track();

    block_count = this->input_to_output(data_track);

    // Write gap sectors
    for(int i = 0; i < 2; i++) {
        edc_outbuf outbuf;
        edc_generate_gap_sector( &outbuf, this->current_lba_ );
        this->outfile_.write(outbuf.data, sizeof(outbuf.data));
        this->current_lba_++;
    }

    return block_count;
}

void
MilCDImageGenerator::build() {
    if (this->get_output_image_mode() == MIL_CD_BUILDER_MODE_INVALID) {
        throw std::logic_error("unrecognized output image file extension");
    }

    // Starting generation process
    this->current_lba_ = EDC_ENCODE_ADDRESS;

    // Open the output file
    this->outfile_ = std::ofstream( this->get_output_file(), std::ofstream::binary );

    // First session
    this->process_audio_tracks( this->get_first_session() );
    this->process_data_track( this->get_first_session() );

    // Second session
    this->process_audio_tracks( this->get_second_session() );
    this->process_data_track( this->get_second_session() );

    // Close the output file (i.e. MDF or CDI)
    // The MDS file is handled elsewhere (TODO)
	this->outfile_.close();
}

void
MilCDImageGenerator::detect_image_mode_from_output_extension() {
    this->mode_ = MIL_CD_BUILDER_MODE_INVALID;

    std::string output_file = this->get_output_file();
    to_upper(output_file);

    if (ends_with(output_file, FILE_EXTENSION_DISCJUGGLER)) {
        this->mode_ = MIL_CD_BUILDER_MODE_DISCJUGGLER;
    } else if (ends_with(output_file, FILE_EXTENSION_ALCOHOL)) {
        this->mode_ = MIL_CD_BUILDER_MODE_ALCOHOL;
    } else if (ends_with(output_file, FILE_EXTENSION_ALCOHOL_HEADER)) {
        this->mode_ = MIL_CD_BUILDER_MODE_ALCOHOL;
        output_file = change_file_extension( output_file, FILE_EXTENSION_ALCOHOL );
        to_lower( output_file );
        this->set_output_file( output_file );
    }
}

// Thanks Ali Alidoust (https://stackoverflow.com/a/40975716/3726096)
std::string
conv_milcd_image_mode_to_string(int value) {
    RETURN_STR(value, MIL_CD_BUILDER_MODE_DISCJUGGLER);
    RETURN_STR(value, MIL_CD_BUILDER_MODE_ALCOHOL);
    RETURN_STR(value, MIL_CD_BUILDER_MODE_INVALID);
    return RETURN_STR_UNKNOWN;
}
