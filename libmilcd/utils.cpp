#include "utils.hpp"

// Thanks to jws: https://stackoverflow.com/a/47488790/3726096
bool
starts_with( std::string const & value, std::string const & prefix ) {
    size_t valueSize = value.size();
    size_t prefixSize = prefix.size();

    if (prefixSize > valueSize) {
        return false;
    }

    return memcmp(value.data(), prefix.data(), prefixSize) == 0;
}

// Thanks to jws: https://stackoverflow.com/a/47488790/3726096
bool
ends_with( std::string const & value, std::string const & suffix ) {
    size_t valueSize = value.size();
    size_t suffixSize = suffix.size();

    if (suffixSize > valueSize) {
        return false;
    }

    const char * valuePtr = value.data() + valueSize - suffixSize;
    return memcmp(valuePtr, suffix.data(), suffixSize) == 0;
}

void
to_lower( std::string & str ) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}

void
to_upper( std::string & str ) {
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

// Thanks to Jobin: https://stackoverflow.com/a/37808094/3726096
std::streamsize
get_file_size( std::istream & infile ) {
    std::streampos pos = infile.tellg();
    infile.ignore( std::numeric_limits<std::streamsize>::max() );
    std::streamsize input_length = infile.gcount();
    infile.clear();
    infile.seekg( pos, std::ios_base::beg );
    return input_length;
}

void
write_null_block( std::ostream & outfile, unsigned long block_size ) {
    std::unique_ptr<char[]> buffer { new (std::nothrow) char[block_size] { } };
    outfile.write(buffer.get(), block_size);
}

// Thanks to dvhamme: https://stackoverflow.com/a/27796271/3726096
std::string
change_file_extension( std::string const & filename, std::string const& extension ) {
    return filename.substr(0, filename.find_last_of('.')) + '.' + extension;
}
