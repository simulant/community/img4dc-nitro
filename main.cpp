#include <iostream>
#include <kazsignal/kazsignal.h>

#include "libmilcd.hpp"

int main()
{
    MilCDImageGenerator milcd;

    int call_count = 0;
    sig::scoped_connection conn = milcd.progress_signal().connect([&call_count](std::streamsize current_bytes, std::streamsize total_bytes, unsigned int block_count) {
        call_count++;
        std::cout << current_bytes << "/" << total_bytes << " (" << block_count << ")" << std::endl;
    });

    // Adding 3 audio tracks in the first session
    milcd.get_first_session().add_audio_track("debug/sources/audio_1.raw");
//    milcd.get_first_session().add_audio_track("debug/sources/audio_2.raw");
//    milcd.get_first_session().add_audio_track("debug/sources/audio_3.raw");

    // Adding the appropriate data track (with the correct multi-session information - MSINFO)
    milcd.get_second_session().set_data_track("debug/sources/audio_data_cdda_1.iso");

    // Generating an Alcohol image (MDS/MDF)
    // TODO: Currently, the MDF is partially generated but with the MDS extension, which isn't correct of course
    //       This will be changed later
    milcd.set_output_file("bin/Debug/selfboot.mds");

    // Some debug information
    std::cout << "*** START ***" << std::endl;
    std::cout << "Input file: " << milcd.get_second_session().get_data_track().get_file_name() << std::endl;
    std::cout << "Output file: " << milcd.get_output_file() << std::endl;
    std::cout << "Mode: " << conv_milcd_image_mode_to_string(milcd.get_output_image_mode()) << std::endl;
    std::cout << "*** END ***" << std::endl;

    // Do the magic!
    milcd.build();

    return 0;
}
