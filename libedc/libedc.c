#include "libedc.h"

int edc_encode_sector( edc_data *data, unsigned int address ) {
    memset( data->outbuf, 0, EDC_SECTOR_SIZE_DECODE );
    memcpy( data->outbuf + EDC_LOAD_OFFSET, data->inbuf, EDC_SECTOR_SIZE_ENCODE );
    return do_encode_L2( (unsigned char *) data->outbuf, MODE_2_FORM_1, address );
}

void edc_generate_gap_sector( edc_outbuf *outbuf, unsigned int address ) {
    memset( outbuf, 0, EDC_SECTOR_SIZE_DECODE );

    do_encode_L2( (unsigned char *) outbuf, MODE_2_FORM_1, address );

    unsigned int offset = EDC_SECTOR_SIZE_DECODE - 4;
    unsigned long magic = 0xbeb0133f;
    outbuf->data[offset+0] = magic >> 0L;
    outbuf->data[offset+1] = magic >> 8L;
    outbuf->data[offset+2] = magic >> 16L;
    outbuf->data[offset+3] = magic >> 24L;
}
