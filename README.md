# IMG4DC-Nitro: IMG4DC Reimagined
**IMG4DC** is a tool written for generating selfboot disc images for the **Sega Dreamcast**, in the **MIL-CD** format.

## Introduction
A selfboot image is a disc image format specially prepared to boot on the Dreamcast. By burning this image to a standard blank CD-R, the software containted into the disc will run on the Dreamcast, as a legit game disc, without any modchip or other hacking device.

To use this tool, you will need to generate an `ISO` image with a compatible tool (`mkisofs` or `genisoimage`).

## Content of this repository
This repository contains the future **IMG4DC** totally rewritten in C++. This project is early-stage! It doesn't contains anything usable now.

## Contribute
To contribute, you may use [Code::Blocks](http://www.codeblocks.org/). Open the `img4dc-nitro.workspace` file in the IDE. In the future, standard `Makefile` will be provided too.

Join our [Discord Channel](https://discordapp.com/channels/488332893324836864/631054084845076480)!

