# Debug Files

These files are debug files used for developing **img4dc-nitro**. They aren't
meant to be released with the tool.

The `sources` directory contains the sources files used for making the images
that are stored in the `targets` directory.

The goal of the **img4dc-nitro** project is to regenerate the same images stored
in the `targets` directory from the `sources` directory.
